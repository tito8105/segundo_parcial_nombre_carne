from django.urls import path,  include
from rest_framework import routers 
from core.telefono import views

router=routers.DefaultRouter()
router.register(r'telefono', views.TelefonoViewSet)

urlpatterns=[

    path('', include(router.urls)),
    path('telefono/',include('rest_framework.urls', namespace='rest_framework'))
]