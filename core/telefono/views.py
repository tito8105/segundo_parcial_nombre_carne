from django.shortcuts import render
from rest_framework import viewsets
from rest_framework import permissions
from .serializers import TelefonoSerializer
from .models import Telefono
from core.telefono.serializers import TelefonoSerializer
from django.contrib.auth.models import User, Group

# Create your views here.

class TelefonoViewSet(viewsets.ModelViewSet):
    queryset =  Telefono.objects.all()
    serializer_class =  TelefonoSerializer

